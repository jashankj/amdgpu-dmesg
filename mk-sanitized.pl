#!/usr/bin/env perl

my $RE_UUID = qr{
	[0-9a-f]{8} - [0-9a-f]{4} - [0-9a-f]{4} - [0-9a-f]{4} - [0-9a-f]{12}
}x;

my $RE_EUI48 = qr{
	[0-9a-f]{2} : [0-9a-f]{2} : [0-9a-f]{2} :
	[0-9a-f]{2} : [0-9a-f]{2} : [0-9a-f]{2}
}x;

while (<>) {
	# Skip journal timestamps; make result (semi-)reproducible.
	next if /^-- Journal begins at /;

	# Pretty much all kernel audit events are not useful.
	next if / kernel: audit: /;

	# Something about the wireless stack's signal strength monitoring
	# means the wireless connection tends to flap.  This tends to be
	# quite noisy in the journal.
	next if / kernel: wlp3s0: /;
	next if / kernel: bond0: /;
	next if / kernel: IPv6: ADDRCONF\(NETDEV_CHANGE\): /;

	# Possibly related to the above.
	next if / kernel: iwlwifi .*: expected hw-decrypted unicast frame for station/;

	# Canonical have put a keylogger into their kernel.
	next if / kernel: evbug: Event. /;

	# Filter out various identifiers.
	s@$RE_UUID@[[--UUID--]]@gi;
	s@$RE_EUI48@[[--EUI48--]]@gi;
	s@SerialNumber: .*@SerialNumber: [[--SERIAL--]]@g;

	# Otherwise, print that line.
	print;
}
