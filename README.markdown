in which jashank is sad about drivers/gpu/drm/amd
========================================================================

Since December 2020,
the AMD GPU driver
has proved to be
the *single most unstable driver*
I've used on any OS
in the last twenty-five years.
Here is a brief taste of what I mean ---

<table>
<thead>
<tr><th>log
    <th>kernel version
    <th>uptime
    <th>shutdown reason
</thead>
<tbody>

<!-- tr><td>dmesg.20210218.085616
    <td>5.10.16 + agd5f/linux@e1d0a7140ee0e82db3f3bad634b767f22a437798
    <td>(hours)
    <td>still up! -->

<tr><td>dmesg.20210218.085507--20210218.085544
    <td>5.10.14-arch1-1
    <td>37s
    <td>(forgot to mkinitcpio)

<tr><td>dmesg.20210217.031201--20210218.085213
    <td>5.10.14-arch1-1
    <td>29h40m12s
    <td><strong>amdgpu crash</strong>, under load, [divide error, Calculate256BBlockSizes][fdo-1344].
      Xorg, picom, several Electron, Firefox, tetrio

<tr><td>dmesg.20210217.003337--20210217.031033
    <td>5.10.14-arch1-1
    <td>2h36m56s
    <td>suspend, no resume

<tr><td>dmesg.20210217.001841--20210217.003159
    <td>5.10.14-arch1-1
    <td>13m18s
    <td><strong>amdgpu crash</strong>, under load, [divide error, CalculateVMAndRowBytes][fdo-1202].
	  trying to teach at the time.

<tr><td>dmesg.20210215.093618--20210217.001722
    <td>5.10.14-arch1-1
    <td>38h41m04s
    <td><strong>amdgpu crash</strong>, under load, hang/crash
      (precursor to a DISPCLK..., probably).
	  trying to teach at the time.

<tr><td>dmesg.20210215.040801--20210215.093444
    <td>5.10.14-arch1-1
    <td>5h26m43s
    <td><strong>amdgpu crash</strong>, under load, [divide error, Calculate256BBlockSizes][fdo-1344]

<tr><td>dmesg.20210211.010631--20210215.040636
    <td>5.10.14-arch1-1
    <td>99h00m05s
    <td><strong>amdgpu crash</strong>, under load, [divide error, CalculateVMAndRowBytes][fdo-1202]

<tr><td>dmesg.20210209.045841--20210211.010457
    <td>5.10.14-arch1-1
    <td>44h06m16s
    <td><strong>amdgpu crash</strong>, under load, [divide error, Calculate256BBlockSizes][fdo-1344]

<tr><td>dmesg.20210207.121703--20210209.045704
    <td>5.10.12-arch1-1
    <td>47h29m46s
    <td><strong>amdgpu crash</strong>, under load, [divide error, Calculate256BBlockSizes][fdo-1344]

<tr><td>dmesg.20210205.124542--20210207.121528
    <td>5.10.12-arch1-1
    <td>47h29m46s
    <td><strong>amdgpu crash</strong>, under load, [divide error, CalculateVMAndRowBytes][fdo-1202]

<tr><td>dmesg.20210205.123735--20210205.124411
    <td>5.10.12-arch1-1
    <td>6m36s
    <td><strong>amdgpu crash</strong>, Vive Cosmos, invalid opcode

<tr><td>dmesg.20210204.123621--20210205.123643
    <td>5.10.12-arch1-1
    <td>42s
    <td><strong>amdgpu crash</strong>, Vive Cosmos, invalid opcode

<tr><td>dmesg.20210204.131158--20210205.123536
    <td>5.10.12-arch1-1
    <td>23h23m38s
    <td><strong>amdgpu crash</strong>, Vive Cosmos, invalid opcode

<tr><td>dmesg.20210204.031821--20210204.130628
    <td>5.10.12-arch1-1
    <td>9h48m07s
    <td><strong>amdgpu crash</strong>, resume, page fault "faulty UTCL2"

<tr><td>dmesg.20210203.231008--20210204.031653
    <td>5.10.12-arch1-1
    <td>4h06m45s
    <td><strong>amdgpu crash</strong>, under load, [divide error, DISPCLKDPPCLKDCFCLKDeepSleepPrefetchParametersWatermarksAndPerformanceCalculation][fdo-1418]

<tr><td>dmesg.20210201.005313--20210203.230927
    <td>5.10.7-arch1-1
    <td>70h16m14s
    <td>rebooting, updates.

<tr><td>dmesg.20210123.234540--20210201.005150
    <td>5.10.7-arch1-1
    <td>193h06m10s
    <td><strong>amdgpu crash</strong>, under load, [divide error, DISPCLKDPPCLKDCFCLKDeepSleepPrefetchParametersWatermarksAndPerformanceCalculation][fdo-1418]

<tr><td>dmesg.20210119.073409--20210123.234428
    <td>5.10.7-arch1-1
    <td>112h10m19s
    <td><strong>amdgpu crash</strong>, under load, [divide error, DISPCLKDPPCLKDCFCLKDeepSleepPrefetchParametersWatermarksAndPerformanceCalculation][fdo-1418]
    (and many other warnings)

<tr><td>dmesg.20210119.045406--20210119.073115
    <td>5.10.7-arch1-1
    <td>2h37m09s
    <td><strong>amdgpu crash</strong>, under load, precursor to [1418][fdo-1418] [1433][fdo-1344] [1202][fdo-1202]

<tr><td>dmesg.20210118.125352--20210119.045317
    <td>5.10.7-arch1-1
    <td>15h59m25s
    <td>(crash? unclear)

<tr><td>dmesg.20210118.064222--20210118.120029
    <td>5.10.7-arch1-1
    <td>5h18m11s
    <td><strong>amdgpu crash</strong>, under load, [divide error, DISPCLKDPPCLKDCFCLKDeepSleepPrefetchParametersWatermarksAndPerformanceCalculation][fdo-1418]

<tr><td>dmesg.20210117.233323--20210118.064127
    <td>5.10.5-arch1-1
    <td>7h08m06s
    <td><strong>amdgpu crash</strong>, under load, [divide error, Calculate256BBlockSizes][fdo-1344]

<tr><td>dmesg.20210117.231059--20210117.233148
    <td>5.10.5-arch1-1
    <td>0h20m51s
    <td><strong>amdgpu crash</strong>, under load, [divide error, DISPCLKDPPCLKDCFCLKDeepSleepPrefetchParametersWatermarksAndPerformanceCalculation][fdo-1418]

<tr><td>dmesg.20210115.050531--20210117.230952
    <td>5.10.5-arch1-1
    <td>19h29m54s
    <td><strong>amdgpu crash</strong>, dock attach, [DP MST NULL dereference][fdo-1420]

<tr><td>dmesg.20210113.122257--20210115.050447
    <td>5.10.5-arch1-1
    <td>16h17m49s
    <td><strong>amdgpu crash</strong>, under load, [divide error, CalculateVMAndRowBytes][fdo-1202]

<tr><td>dmesg.20210113.061609--20210113.112217
    <td>5.10.5-arch1-1
    <td>5h06m11s
    <td>(reboot, bisecting)

<tr><td>dmesg.20210112.093252--20210113.061452
    <td>5.10.5-arch1-1
    <td>11h06m12s
    <td><strong>amdgpu crash</strong>, under load, [divide error, DISPCLKDPPCLKDCFCLKDeepSleepPrefetchParametersWatermarksAndPerformanceCalculation][fdo-1418]

<tr><td>dmesg.20210110.050000--20210112.092433
    <td>5.10.5-arch1-1
    <td>22h03m28s
    <td><strong>amdgpu crash</strong>, under load, [divide error, DISPCLKDPPCLKDCFCLKDeepSleepPrefetchParametersWatermarksAndPerformanceCalculation][fdo-1418]

<tr><td>dmesg.20210110.045442--20210110.045843
    <td>5.10.5-arch1-1
    <td>0h04m04s
    <td><strong>amdgpu crash</strong>, dock attach, [DP MST NULL dereference][fdo-1420]

<tr><td>dmesg.20210110.045037--20210110.045344
    <td>5.10.5-arch1-1
    <td>0h02m28s
    <td><strong>amdgpu crash</strong>, dock attach, [DP MST NULL dereference][fdo-1420]

<tr><td>dmesg.20210110.041800--20210110.045014
    <td>5.10.3-arch1-1
    <td>0h32m18s
    <td>(reboot, bisecting)

<tr><td>dmesg.20210110.040946--20210110.041728
    <td>5.10.3-arch1-1
    <td>0h07m45s
    <td><strong>amdgpu crash</strong>, dock attach, [DP MST NULL dereference][fdo-1420]

<tr><td>dmesg.20210110.040523--20210110.040910
    <td>5.10.3-arch1-1
    <td>0h03m46s
    <td><strong>amdgpu crash</strong>, dock attach, [DP MST NULL dereference][fdo-1420]

<tr><td>dmesg.20210110.035615--20210110.040459
    <td>5.10.4-arch2-1
    <td>0h08m48s
    <td>(reboot, bisecting?)

<tr><td>dmesg.20210110.034728--20210110.035537
    <td>5.10.4-arch2-1
    <td>0h06m19s
    <td><strong>amdgpu crash</strong>, dock attach, [DP MST NULL dereference][fdo-1420]

<tr><td>dmesg.20210108.130920--20210110.031815
    <td>5.10.4-arch2-1
    <td>17h57m47s
    <td>(reboot, bisecting?)

<tr><td>dmesg.20210108.075820--20210108.130841
    <td>5.10.4-arch2-1
    <td>3h43m51s
    <td><strong>amdgpu crash</strong>, under load, [divide error, CalculateVMAndRowBytes][fdo-1202]

<tr><td>dmesg.20210108.061441--20210108.074346
    <td>5.10.4-arch2-1
    <td>1h29m08s
    <td>(reboot, bisecting)

<tr><td>dmesg.20210108.060741--20210108.061044
    <td>5.10.4-arch2-1
    <td>0h03m08s
    <td>(reboot?)

<tr><td>dmesg.20210108.060309--20210108.060543
    <td>5.4.86-1-lts
    <td>0h02m37s
    <td>(reboot, wrong kernel)

<tr><td>dmesg.20210105.111600--20210108.060028
    <td>5.10.4-arch2-1
    <td>20h21m01s
    <td>(reboot, bisecting)

<tr><td>dmesg.20210102.090026--20210105.111534
    <td>5.10.2-arch1-1
    <td>43h31m11s
    <td>(reboot; lots of PM bugs)

<tr><td>dmesg.20210102.071212--20210102.081456
    <td>5.10.2-arch1-1
    <td>1h02m46s
    <td>(reboot, bisecting)

<tr><td>dmesg.20210102.061758--20210102.071025
    <td>5.10.2-arch1-1
    <td>0h52m29s
    <td>(reboot, bisecting)

<tr><td>dmesg.20210102.053107--20210102.061538
    <td>5.10.2-arch1-1
    <td>0h44m33s
    <td>(reboot, bisecting)

<tr><td>dmesg.20210102.050408--20210102.052926
    <td>5.10.2-arch1-1
    <td>0h25m20s
    <td>(reboot, bisecting)

<tr><td>dmesg.20210102.042415--20210102.050213
    <td>5.10.2-arch1-1
    <td>0h38m00s
    <td>(reboot, bisecting)

<tr><td>dmesg.20210101.120223--20210102.042156
    <td>5.10.2-arch1-1
    <td>3h11m02s
    <td>(reboot, bisecting)

<tr><td>dmesg.20210101.105735--20210101.115946
    <td>5.10.2-arch1-1
    <td>1h02m13s
    <td>(reboot, bisecting)

<tr><td>dmesg.20210101.094813--20210101.105538
    <td>5.10.2-arch1-1
    <td>1h07m27s
    <td>(reboot, bisecting)

<tr><td>dmesg.20210101.091741--20210101.094407
    <td>5.10.2-arch1-1
    <td>0h26m28s
    <td>(reboot, bisecting)

<tr><td>dmesg.20210101.091702--20210101.091702
    <td>5.4.85-1-lts
    <td>0h00m02s
    <td>(reboot, wrong kernel)

<tr><td>dmesg.20210101.081949--20210101.091505
    <td>5.10.2-arch1-1
    <td>0h55m17s
    <td>(reboot, bisecting)

<tr><td>dmesg.20210101.080403--20210101.081657
    <td>5.10.2-arch1-1
    <td>0h12m56s
    <td>(reboot, bisecting)

<tr><td>dmesg.20201231.094650--20210101.075541
    <td>5.10.2-arch1-1
    <td>12h21m48s
    <td><strong>amdgpu crash</strong>, under load, [divide error, CalculateVMAndRowBytes][fdo-1202]

<tr><td>dmesg.20201231.072557--20201231.093940
    <td>5.10.2-arch1-1
    <td>2h13m45s
    <td>(reboot, bisecting)

<tr><td>dmesg.20201231.072445--20201231.072522
    <td>5.4.85-1-lts
    <td>0h00m39s
    <td>(reboot, wrong kernel)

<tr><td>dmesg.20201231.064600--20201231.071942
    <td>5.10.2-arch1-1
    <td>0h33m45s
    <td>(reboot, bisecting)

<tr><td>dmesg.20201231.052850--20201231.064240
    <td>5.10.2-arch1-1
    <td>1h13m52s
    <td><strong>amdgpu crash</strong>, under load, [divide error, CalculateVMAndRowBytes][fdo-1202]

<tr><td>dmesg.20201231.040226--20201231.052608
    <td>5.10.2-arch1-1
    <td>1h23m44s
    <td><strong>amdgpu crash</strong>, under load, [divide error, Calculate256BBlockSizes][fdo-1344]

<tr><td>dmesg.20201231.024357--20201231.040007
    <td>5.10.2-arch1-1
    <td>1h16m42
    <td>(reboot, bisecting)

<tr><td>dmesg.20201231.012944--20201231.023927
    <td>5.10.2-arch1-1
    <td>1h09m46s
    <td>(reboot, bisecting)

<tr><td>dmesg.20201230.121516--20201231.012717
    <td>5.10.2-arch1-1
    <td>13h12m03s
    <td>(reboot, bisecting)

<tr><td>dmesg.20201230.121410--20201230.121441
    <td>5.4.85-1-lts
    <td>0h00m32s
    <td>(reboot, wrong kernel)

<tr><td>dmesg.20201230.115540--20201230.120202
    <td>5.10.2-arch1-1
    <td>0h06m24s
    <td>(reboot, bisecting)

<tr><td>dmesg.20201230.093433--20201230.115317
    <td>5.10.2-arch1-1
    <td>2h18m46s
    <td>(bisect foiled by stuffed disk)

<tr><td>dmesg.20201230.091050--20201230.093310
    <td>5.10.2-arch1-1
    <td>0h21m28s
    <td><strong>amdgpu crash</strong>, dock attach, [DP MST NULL dereference][fdo-1420]

<tr><td>dmesg.20201230.090139--20201230.090711
    <td>5.10.0
    <td>0h05m43s
    <td>suspend, no resume

<tr><td>dmesg.20201230.085130--20201230.085955
    <td>5.11.0-rc1
    <td>0h08m35s
    <td>suspend, no resume

<tr><td>dmesg.20201230.084058--20201230.084934
    <td>5.10.0
    <td>0h08m46s
    <td><strong>amdgpu crash</strong>, dock attach, [DP MST NULL dereference][fdo-1420]

<tr><td>dmesg.20201230.083724--20201230.084023
    <td>5.11.0-rc1
    <td>0h03m09s
    <td>(?)

<tr><td>dmesg.20201229.125804--20201230.083547
    <td>5.10.2-arch1-1
    <td>8h56m09s
    <td><strong>amdgpu crash</strong>, under load, [divide error, CalculateVMAndRowBytes][fdo-1202]

<tr><td>dmesg.20201229.125721--20201229.125746
    <td>5.8.0-33-generic
    <td>0h00m29s
    <td>(reboot, wrong kernel)

<tr><td>dmesg.20201229.123932--20201229.125616
    <td>5.11.0-rc1
    <td>0h16m53s
    <td><strong>amdgpu crash</strong>, invalid opcode?!

<tr><td>dmesg.20201229.123554--20201229.123818
    <td>5.11.0-rc1
    <td>
    <td>

<tr><td>dmesg.20201229.123209--20201229.123345
    <td>5.11.0-rc1
    <td>
    <td>

<tr><td>dmesg.20201229.115434--20201229.123040
    <td>5.10.2-arch1-1
    <td>0h36m07s
    <td><strong>amdgpu crash</strong>, dock attach, [DP MST NULL dereference][fdo-1420]

<tr><td>dmesg.20201229.114932--20201229.115303
    <td>5.10.2-arch1-1
    <td>0h03m33s
    <td><strong>amdgpu crash</strong>, dock attach, [DP MST NULL dereference][fdo-1420]

<tr><td>dmesg.20201229.113646--20201229.114756
    <td>5.10.2-arch1-1
    <td>0h11m12s
    <td>?crash on resume

<tr><td>dmesg.20201229.113338--20201229.113624
    <td>5.8.0-33-generic
    <td>0h02m49s
    <td>(reboot, wrong kernel)

<tr><td>dmesg.20201229.111343--20201229.113228
    <td>5.10.2-arch1-1
    <td>0h18m35s
    <td><strong>amdgpu crash</strong>, dock attach, [DP MST NULL dereference][fdo-1420]

<tr><td>dmesg.20201229.105430--20201229.111252
    <td>5.10.2-arch1-1
    <td>
    <td><strong>amdgpu crash</strong>, dock attach, probably DP MST related

<tr><td>dmesg.20201229.092711--20201229.093047
    <td>5.10.2-arch1-1
    <td>0h02m37s
    <td>(log doesn't end?)

<tr><td>dmesg.20201229.092057--20201229.092636
    <td>5.10.2-arch1-1
    <td>
    <td>

<tr><td>dmesg.20201229.074434--20201229.091959
    <td>5.11.0-rc1
    <td>1h35m38s
    <td>(?)

<tr><td>dmesg.20201229.073922--20201229.074401
    <td>5.11.0-rc1
    <td>
    <td>

<tr><td>dmesg.20201229.073502--20201229.073900
    <td>5.11.0-rc1
    <td>0h04m08s
    <td>(normal reboot)

<tr><td>dmesg.20201229.060759--20201229.073437
    <td>5.11.0-rc1
    <td>1h26m48s
    <td>(normal reboot?)

<tr><td>dmesg.20201229.050544--20201229.060733
    <td>5.11.0-rc1
    <td>1h01m59s
    <td>(normal reboot?)

<tr><td>dmesg.20201228.063039--20201228.124413
    <td>5.11.0-rc1
    <td>6h13m43s
    <td>suspend, no resume

<tr><td>dmesg.20201228.011909--20201228.063006
    <td>Ubuntu 5.8.0-33.36-generic
    <td>5h11m00s
    <td>(normal reboot)

<tr><td>dmesg.20201226.045047--20201228.010552
    <td>Ubuntu 5.8.0-33.36-generic
    <td>29h31m04s
    <td>(oom'd, my bad)

<tr><td>dmesg.20201226.040935--20201226.045027
    <td>5.10.2-arch1-1
    <td>0h40m56s
    <td>(picking up a kernel)

<tr><td>dmesg.20201226.034313--20201226.040123
    <td>5.4.85-1-lts
    <td>0h18m13s
    <td>(unsupported card)

<tr><td>dmesg.20201226.031731--20201226.034136
    <td>5.10.2-arch1-1
    <td>0h10m58s
    <td><strong>amdgpu crash</strong>, [DP MST NULL dereference][fdo-1420]

<tr><td>dmesg.20201226.014649--20201226.031548
    <td>5.9.14-arch1-1
    <td>1h29m02s
    <td><strong>amdgpu crash</strong>, [divide error, CalculateVMAndRowBytes][fdo-1202]

</tbody>
</table>

[fdo-348]: https://gitlab.freedesktop.org/drm/amd/-/issues/348
[fdo-745]: https://gitlab.freedesktop.org/drm/amd/-/issues/745
[fdo-1202]: https://gitlab.freedesktop.org/drm/amd/-/issues/1202
[fdo-1306]: https://gitlab.freedesktop.org/drm/amd/-/issues/1306
[fdo-1337]: https://gitlab.freedesktop.org/drm/amd/-/issues/1337
[fdo-1344]: https://gitlab.freedesktop.org/drm/amd/-/issues/1344
[fdo-1358]: https://gitlab.freedesktop.org/drm/amd/-/issues/1358
[fdo-1360]: https://gitlab.freedesktop.org/drm/amd/-/issues/1360
[fdo-1418]: https://gitlab.freedesktop.org/drm/amd/-/issues/1418
[fdo-1420]: https://gitlab.freedesktop.org/drm/amd/-/issues/1420
[fdo-1423]: https://gitlab.freedesktop.org/drm/amd/-/issues/1423

