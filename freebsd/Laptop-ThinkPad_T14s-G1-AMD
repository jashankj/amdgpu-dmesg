#language en
#pragma description Guides on how to install and configure FreeBSD on the Lenovo ThinkPad T14s Gen1 AMD.
#pragma keywords FreeBSD, Laptop, hardware, support
#pragma section-numbers 2
##
## Use "----" to separate "==" headers, "-----" to separate "===" and so on.

## Uncomment if you'd like to add a navigation bar for subpages.
## <<Navigation(children)>>

= ThinkPad T14s Gen1 AMD (20UH, 20UJ) =

## Uncomment to add a table of contents.
## <<TableOfContents()>>

##  * Year introduced: 2020 June

----

== Hardware ==

 * CPU: AMD Ryzen 5 Pro 4650U or AMD Ryzen 7 Pro 4750U
 * Ethernet: Realtek RTL8111 GigE [re(4)] + RTL8251/8153 phy [rgephy(4)]
   (no RJ45 -- uses external adapter, a la some pccards)
 * Graphics: AMD Radeon RX Vega 7
 * Memory: 8G or 16G or 32G
 * Screen: 14in 1920x1080 LCD
 * Storage: 128GB or 256GB or 512GB NVMe SSD
 * Wireless: Intel "Wi-Fi 6" AX200

----

== Support overview ==

## Help on tables: see the HelpOnEditing link at the bottom of the lilac page below
||<-2:#d7d7d7> '''Component''' ||<:#d7d7d7> '''Status''' ||<:#d7d7d7> '''Details''' ||
## Does the machine run programs like Xorg just fine?
||<|2(> Graphics  || Graphical sessions             ||<:> /!\ || ||
## Is it possible to control the brightness level of the screen?
||                   Backlight (brightness) control ||<:> /!\ || ||
||<|4(> Input devices || Keyboard backlight ||<:> (./) || ||
## Does scrolling work?
||                       Touchpad           ||<:>  || ||
||                       Touch screen       ||<:>  || ||
||                       Trackpoint         ||<:>  || ||
## Does the media keys for brightness control work?
||<|2(> Media keys  || Brightness keys  ||<:> /!\ || ||
||                     Volume keys      ||<:> /!\ || ||
||<|2(> Network  || Ethernet ||<:> (./) ||  ||
||                  Wi-Fi    ||<:> {X} || unsupported chipset (iwm(4), probably) ||
||<|9(> Other || Battery             ||<:> (./) || runs; haven't tested lifetime ||
||               Bluetooth           ||<:> /!\ || ||
||               Fan                 ||<:> (./) || haven't tested fan control  ||
||               Fingerprint reader  ||<:> /!\ || ||
||               SD card reader      ||<:> /!\ || rtsx(4) stalls boot ||
||               SIM card slot       ||<:> /!\ || ||
||               Smart card reader   ||<:> /!\ || ||
||               Suspend & resume    ||<:> {X} || suspends; no resume ||
||               Webcam              ||<:> /!\ || ||
||<|2(> Ports || HDMI         ||<:> /!\ || ||
||               USB          ||<:> (./) || boot from USB; 2x A, 2x C ||
||<|2(> Sound || Headset jack     ||<:> /!\ || ||
||               Speakers         ||<:> (./) || beeps happily ||
||<-4:> ~-Last update: 2020-12-21, 19ebb3aa230-c255150 -~ ||

 Legend::
 :: (./) OK
 :: {i} Somewhat OK: requires additional tweaking or works worse than expected
 :: /!\ Present but status unknown or untested
 :: {X} Does not work
 :: ''nothing'' Not sure if present

* Boots from UEFI with vt(4); 1920x1080 via efifb
* acpi_video(4) provides LCD backlight controls
* amdtemp(4) works

## ----
##
## == Tweaks ==

----

== Files ==

## Remember to submit your dmesg to http://dmesgd.nycbug.org.
##
## The "AttachList" macro will expand to the list of files attached to the wiki page. No need to list them manually.
<<AttachList(@PAGE@)>>

----

CategoryLaptop CategoryLenovo CategoryThinkpad