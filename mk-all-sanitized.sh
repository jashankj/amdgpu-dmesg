journalctl --utc --list-boots \
| sed -E 's@—@ @; s@... (....)-(..)-(..) (..):(..):(..) UTC@\1\2\3.\4\5\6@g' \
| awk '{print "journalctl --output=short-monotonic --no-hostname -kb " $2 " | perl mk-sanitized.pl > all-sanitized/dmesg." $3 "--" $4 }' \
| xargs -t -P4 -L1 -I% sh -c '%'
